package edu.csu.csuhumanresourcesapp.contract;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;
import edu.csu.csuhumanresourcesapp.contract.BaseClass;
import io.restassured.module.mockmvc.specification.MockMvcRequestSpecification;
import io.restassured.response.ResponseOptions;
import java.io.StringReader;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.junit.Test;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;

import static com.toomuchcoding.jsonassert.JsonAssertion.assertThatJson;
import static io.restassured.module.mockmvc.RestAssuredMockMvc.*;
import static org.springframework.cloud.contract.verifier.assertion.SpringCloudContractAssertions.assertThat;
import static org.springframework.cloud.contract.verifier.util.ContractVerifierUtil.*;

public class ContractVerifierTest extends BaseClass {

	@Test
	public void validate_delete_csu_employee_request() throws Exception {
		// given:
			MockMvcRequestSpecification request = given();

		// when:
			ResponseOptions response = given().spec(request)
					.delete("/csu-employees/1");

		// then:
			assertThat(response.statusCode()).isEqualTo(200);
	}

	@Test
	public void validate_delete_csu_employee_request_with_out_of_range_id() throws Exception {
		// given:
			MockMvcRequestSpecification request = given();

		// when:
			ResponseOptions response = given().spec(request)
					.delete("/csu-employees/2");

		// then:
			assertThat(response.statusCode()).isEqualTo(404);
	}

	@Test
	public void validate_get_all_csu_employees_request() throws Exception {
		// given:
			MockMvcRequestSpecification request = given()
					.header("Content-Type", "application/json")
					.header("Accept", "application/json");

		// when:
			ResponseOptions response = given().spec(request)
					.get("/csu-employees");

		// then:
			assertThat(response.statusCode()).isEqualTo(200);
			assertThat(response.header("Content-Type")).isEqualTo("application/json;charset=UTF-8");
		// and:
			DocumentContext parsedJson = JsonPath.parse(response.getBody().asString());
			assertThatJson(parsedJson).array().contains("['department']").isEqualTo("CS");
			assertThatJson(parsedJson).array().contains("['employeeId']").isEqualTo("2");
			assertThatJson(parsedJson).array().contains("['employeeId']").isEqualTo("1");
			assertThatJson(parsedJson).array().contains("['firstName']").isEqualTo("Sean");
			assertThatJson(parsedJson).array().contains("['firstName']").isEqualTo("Paul");
			assertThatJson(parsedJson).array().contains("['jobTitle']").isEqualTo("Professor of Humanities");
			assertThatJson(parsedJson).array().contains("['jobTitle']").isEqualTo("Professor of Computer Science");
			assertThatJson(parsedJson).array().contains("['lastName']").isEqualTo("West");
			assertThatJson(parsedJson).array().contains("['department']").isEqualTo("HIST");
			assertThatJson(parsedJson).array().contains("['lastName']").isEqualTo("Hayes");
	}

	@Test
	public void validate_get_specific_csu_employee_request() throws Exception {
		// given:
			MockMvcRequestSpecification request = given()
					.header("Content-Type", "application/json");

		// when:
			ResponseOptions response = given().spec(request)
					.get("/csu-employees/1");

		// then:
			assertThat(response.statusCode()).isEqualTo(200);
			assertThat(response.header("Content-Type")).isEqualTo("application/json;charset=UTF-8");
		// and:
			DocumentContext parsedJson = JsonPath.parse(response.getBody().asString());
			assertThatJson(parsedJson).field("['jobTitle']").isEqualTo("Professor of Computer Science");
			assertThatJson(parsedJson).field("['firstName']").isEqualTo("Sean");
			assertThatJson(parsedJson).field("['employeeId']").isEqualTo(1);
			assertThatJson(parsedJson).field("['department']").isEqualTo("CS");
			assertThatJson(parsedJson).field("['lastName']").isEqualTo("Hayes");
	}

	@Test
	public void validate_get_csu_employee_request_with_out_of_range_id() throws Exception {
		// given:
			MockMvcRequestSpecification request = given();

		// when:
			ResponseOptions response = given().spec(request)
					.delete("/csu-employees/2");

		// then:
			assertThat(response.statusCode()).isEqualTo(404);
	}

	@Test
	public void validate_post_csu_employee_request() throws Exception {
		// given:
			MockMvcRequestSpecification request = given()
					.header("Content-Type", "application/json")
					.body("{\"employeeId\":2,\"firstName\":\"Paul\",\"lastName\":\"West\",\"jobTitle\":\"Professor of Humanities\",\"department\":\"HIST\"}");

		// when:
			ResponseOptions response = given().spec(request)
					.post("/csu-employees");

		// then:
			assertThat(response.statusCode()).isEqualTo(201);
			assertThat(response.header("Content-Type")).isEqualTo("application/json;charset=UTF-8");
		// and:
			DocumentContext parsedJson = JsonPath.parse(response.getBody().asString());
			assertThatJson(parsedJson).field("['firstName']").isEqualTo("Paul");
			assertThatJson(parsedJson).field("['jobTitle']").isEqualTo("Professor of Humanities");
			assertThatJson(parsedJson).field("['lastName']").isEqualTo("West");
			assertThatJson(parsedJson).field("['employeeId']").isEqualTo(2);
			assertThatJson(parsedJson).field("['department']").isEqualTo("HIST");
	}

	@Test
	public void validate_post_request_with_missing_firstName_field_that_returns_a_400_status_code() throws Exception {
		// given:
			MockMvcRequestSpecification request = given()
					.header("Content-Type", "application/json")
					.body("{\"employeeId\":1,\"firstName\":null,\"lastName\":\"Daughtry\",\"jobTitle\":\"Professor of Computer Science\",\"department\":\"CS\"}");

		// when:
			ResponseOptions response = given().spec(request)
					.post("/csu-employees");

		// then:
			assertThat(response.statusCode()).isEqualTo(400);
	}

	@Test
	public void validate_post_request_with_missing_lastName_field_that_returns_a_400_status_code() throws Exception {
		// given:
			MockMvcRequestSpecification request = given()
					.header("Content-Type", "application/json")
					.body("{\"employeeId\":1,\"firstName\":\"Cher\",\"lastName\":null,\"jobTitle\":\"Professor of Computer Science\",\"department\":\"CS\"}");

		// when:
			ResponseOptions response = given().spec(request)
					.post("/csu-employees");

		// then:
			assertThat(response.statusCode()).isEqualTo(400);
	}

	@Test
	public void validate_put_csu_employee_request() throws Exception {
		// given:
			MockMvcRequestSpecification request = given()
					.header("Content-Type", "application/json")
					.body("{\"employeeId\":1,\"firstName\":\"Sean\",\"lastName\":\"Hayes\",\"jobTitle\":\"Professor of Computer Science\",\"department\":\"CS\"}");

		// when:
			ResponseOptions response = given().spec(request)
					.put("/csu-employees/1");

		// then:
			assertThat(response.statusCode()).isEqualTo(200);
			assertThat(response.header("Content-Type")).isEqualTo("application/json;charset=UTF-8");
		// and:
			DocumentContext parsedJson = JsonPath.parse(response.getBody().asString());
			assertThatJson(parsedJson).field("['jobTitle']").isEqualTo("Professor of Computer Science");
			assertThatJson(parsedJson).field("['firstName']").isEqualTo("Sean");
			assertThatJson(parsedJson).field("['employeeId']").isEqualTo(1);
			assertThatJson(parsedJson).field("['department']").isEqualTo("CS");
			assertThatJson(parsedJson).field("['lastName']").isEqualTo("Hayes");
	}

	@Test
	public void validate_put_request_with_missing_firstName_field_that_returns_a_400_status_code() throws Exception {
		// given:
			MockMvcRequestSpecification request = given()
					.header("Content-Type", "application/json")
					.body("{\"employeeId\":1,\"firstName\":null,\"lastName\":\"Daughtry\",\"jobTitle\":\"Professor of Computer Science\",\"department\":\"CS\"}");

		// when:
			ResponseOptions response = given().spec(request)
					.put("/csu-employees/2");

		// then:
			assertThat(response.statusCode()).isEqualTo(400);
	}

	@Test
	public void validate_put_request_with_missing_lastName_field_that_returns_a_400_status_code() throws Exception {
		// given:
			MockMvcRequestSpecification request = given()
					.header("Content-Type", "application/json")
					.body("{\"employeeId\":1,\"firstName\":\"Cher\",\"lastName\":null,\"jobTitle\":\"Professor of Computer Science\",\"department\":\"CS\"}");

		// when:
			ResponseOptions response = given().spec(request)
					.put("/csu-employees/3");

		// then:
			assertThat(response.statusCode()).isEqualTo(400);
	}

}

# Naming Tests

I personally like to provide my test names with the method name, what it returns, and the 
condition that caused the result. So something like this:

```
methodName_result_condition
```

Here is an example of a test that test the method `createProfile()` that returns x 
when y is performed.

```
createProfile_returnsCorrectProfileResponse_whenCiamRespnoseIsEmpty()
```

# Running Tests

Testing all tests in specific package
```
gradle clean test --tests edu.csu.csuhumanresourcesapp*
```


Testing all unit tests
```
gradle clean test --tests *unit*
```




## `@InjectMocks`
much like autowiring but injects beans marked with `@Mock` 
into this bean.

## `@Mock`
allows you to mock the bean marked with this annotation
where you can then use the `when()` method to mock the behavior
of this bean.

_Example_
```
    @Mock
    CsuEmployeeService csuEmployeeService; 

    @InjectMocks
    CsuEmployeeController csuEmployeeController; 
```


Add a service that talks to an outside service that is not a repository

https://jsonplaceholder.typicode.com/posts/1


## Testing MockMvc Responses

Add the following static dependencies for matching response and matchers:
```
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
```

**Status Codes**

* 200 OK
    ```
    .andExpect(status().isCreated())
    ```

* 201 CREATED
    ```
    .andExpect(status().isOk())
    ```

**Content Type**

* application/json
    ```
    .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
    ```

**Testing JSON Response Body**

* Array of JSON objects contains _`n`_ number of objects.
    ```
    .andExpect(jsonPath("$" , hasSize(5)))
    ```
    
* JSON object has _`n`_ number of fields.
    ```
    .andExpect(jsonPath("$.*" , hasSize(5)))
    ```
      
* Embedded JSON object has _`n`_ number of fields.
   ```
   .andExpect(jsonPath("$[0].*" , hasSize(5)))
   ``` 
   
* Field Value Comparision
    ```
    .andExpect(jsonPath("$.firstName", is("Mylynka")))
    ```
    
* Embedded-Object Field Value Comparision
    ```
    .andExpect(jsonPath("$[0].firstName", is("Paul")))
    ```
        
## Running contract tests
`./gradlew generateContractTests`


running only contract tests
`gradle clean test --tests *contract*`


*wire mock
https://www.baeldung.com/introduction-to-wiremock


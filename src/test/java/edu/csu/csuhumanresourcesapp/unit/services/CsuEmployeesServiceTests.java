package edu.csu.csuhumanresourcesapp.unit.services;
import edu.csu.csuhumanresourcesapp.entities.CsuEmployee;
import edu.csu.csuhumanresourcesapp.exceptions.CsuEmployeeNotFoundException;
import edu.csu.csuhumanresourcesapp.exceptions.FieldMissingException;
import edu.csu.csuhumanresourcesapp.repositories.CsuEmployeeRepository;
import edu.csu.csuhumanresourcesapp.services.CsuEmployeeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;
import static edu.csu.csuhumanresourcesapp.ApplicationTests.*;

@RunWith(MockitoJUnitRunner.Silent.class)
@SpringBootTest
public class CsuEmployeesServiceTests {

    @Mock
    CsuEmployeeRepository csuEmployeeRepository;

    @InjectMocks
    CsuEmployeeService csuEmployeeService;

    //GET (1)
    @Test
    public void getCsuEmployee_returnsSpecificCsuEmployee_whenCallIsValid(){
        CsuEmployee csuEmployee = SEAN_HAYES;
        when(csuEmployeeRepository.findById(1L)).thenReturn(Optional.of(csuEmployee));
        CsuEmployee result = csuEmployeeService.getCsuEmployee(1L);
        assertEquals("Hayes", result.getLastName());
    }

    @Test(expected = CsuEmployeeNotFoundException.class)
    public void getCsuEmployee_returnsCsuEmployeeNotFoundException_whenArgumentOutOfRange(){
        when(csuEmployeeRepository.findById(2L)).thenThrow(CsuEmployeeNotFoundException.class);
        CsuEmployee result = csuEmployeeService.getCsuEmployee(2L);
    }

    //GET (all)
    @Test
    public void getAllCsuEmployees_returnsAllCsuEmployees_whenCallIsValid(){
        List<CsuEmployee> mockAllCsuEmployees =  Arrays.asList(new CsuEmployee(), new CsuEmployee());
        when(csuEmployeeRepository.findAll()).thenReturn(mockAllCsuEmployees);
        List<CsuEmployee> allCsuEmployees = csuEmployeeService.getAllCsuEmployees();
        int result = allCsuEmployees.size();
        assertEquals(2, result);
    }

    //POST
    @Test
    public void saveCsuEmployee_returnsSavedCsuEmployee_whenCallIsValid(){
        CsuEmployee newCsuEmployee = PAUL_WEST;
        when(csuEmployeeRepository.save(newCsuEmployee)).thenReturn(newCsuEmployee);
        CsuEmployee result = csuEmployeeService.saveCsuEmployee(newCsuEmployee);
        assertEquals("Paul",result.getFirstName());
    }

    @Test(expected = FieldMissingException.class)
    public void saveCsuEmployee_returnsFieldMissingException_whenFirstNameFieldIsMissing(){
        CsuEmployee savedCsuEmployee = CHER;
        when(csuEmployeeRepository.save(savedCsuEmployee)).thenReturn(savedCsuEmployee);
        csuEmployeeService.saveCsuEmployee(savedCsuEmployee);
    }

    @Test(expected = FieldMissingException.class)
    public void saveCsuEmployee_returnsFieldMissingException_whenLastNameFieldIsMissing(){
        CsuEmployee savedCsuEmployee = DAUGHTRY;
        when(csuEmployeeRepository.save(savedCsuEmployee)).thenReturn(savedCsuEmployee);
        csuEmployeeService.saveCsuEmployee(savedCsuEmployee);
    }

    //PUT
    @Test
    public void updateCsuEmployee_returnsUpdatedCsuEmployee_whenCallIsValid(){
        CsuEmployee updatedCsuEmployee = SEAN_HAYES;
        when(csuEmployeeRepository.existsById(1L)).thenReturn(true);
        when(csuEmployeeRepository.save(updatedCsuEmployee)).thenReturn(updatedCsuEmployee);
        CsuEmployee result = csuEmployeeService.updateCsuEmployee(1L, updatedCsuEmployee);
        assertEquals("Sean",result.getFirstName());
    }

    @Test(expected = FieldMissingException.class)
    public void updateCsuEmployee_returnsFieldMissingException_whenFirstNameFieldIsMissing(){
        CsuEmployee savedCsuEmployee = CHER;
        when(csuEmployeeRepository.save(savedCsuEmployee)).thenReturn(savedCsuEmployee);
        csuEmployeeService.saveCsuEmployee(savedCsuEmployee);
    }

    @Test(expected = FieldMissingException.class)
    public void updateCsuEmployee_returnsFieldMissingException_whenLastNameFieldIsMissing(){
        CsuEmployee savedCsuEmployee = DAUGHTRY;
        when(csuEmployeeRepository.save(savedCsuEmployee)).thenReturn(savedCsuEmployee);
        csuEmployeeService.saveCsuEmployee(savedCsuEmployee);
    }

    //DELETE
    @Test
    public void deleteCsuEmployee_doesNothing_whenCallIsValid(){
        doNothing().when(csuEmployeeRepository).deleteById(1L);
        when(csuEmployeeRepository.existsById(1L)).thenReturn(true);
        csuEmployeeService.deleteCsuEmployee(1L);
        verify(csuEmployeeRepository, times(1)).deleteById(1L);
    }

    @Test (expected = CsuEmployeeNotFoundException.class)
    public void deleteCsuEmployee_returnsCsuEmployeeNotFoundException_whenArgumentOutOfRange(){
        doThrow(FieldMissingException.class).when(csuEmployeeRepository).deleteById(-1L);
        csuEmployeeService.deleteCsuEmployee(-1L);
    }
}

//https://stackoverflow.com/questions/42947613/how-to-resolve-unneccessary-stubbing-exception
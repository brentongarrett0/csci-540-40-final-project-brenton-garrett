package edu.csu.csuhumanresourcesapp.unit.controllers;
import edu.csu.csuhumanresourcesapp.controllers.CsuEmployeeController;
import edu.csu.csuhumanresourcesapp.entities.CsuEmployee;
import edu.csu.csuhumanresourcesapp.exceptions.CsuEmployeeNotFoundException;
import edu.csu.csuhumanresourcesapp.exceptions.FieldMissingException;
import edu.csu.csuhumanresourcesapp.services.CsuEmployeeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.Arrays;
import java.util.List;
import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static edu.csu.csuhumanresourcesapp.ApplicationTests.*;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class CsuEmployeeControllerTests {

    @Mock
    CsuEmployeeService csuEmployeeService;

    @InjectMocks
    CsuEmployeeController csuEmployeeController;

    //GET (1)
    @Test
    public void getCsuEmployee_returnsSpecificCsuEmployee_whenCallIsValid(){
        when(csuEmployeeService.getCsuEmployee(1L)).thenReturn(SEAN_HAYES);
        CsuEmployee result = csuEmployeeController.getCsuEmployee(1L);
        assertEquals("Sean", result.getFirstName());
    }

    @Test(expected = CsuEmployeeNotFoundException.class)
    public void getCsuEmployee_returnsCsuEmployeeNotFoundException_whenArgumentOutOfRange(){
        when(csuEmployeeService.getCsuEmployee(2L)).thenThrow(CsuEmployeeNotFoundException.class);
        CsuEmployee result = csuEmployeeController.getCsuEmployee(2L);
    }

    //GET (all)
    @Test
    public void getAllCsuEmployees_returnsAllCsuEmployees_whenCallIsValid(){
        List<CsuEmployee> mockAllCsuEmployees =  Arrays.asList(new CsuEmployee(), new CsuEmployee());
        when(csuEmployeeService.getAllCsuEmployees()).thenReturn(mockAllCsuEmployees);
        List<CsuEmployee> allCsuEmployees = csuEmployeeController.getAllCsuEmployees();
        int result = allCsuEmployees.size();
        assertEquals(2, result);
    }

    //POST
    @Test
    public void saveCsuEmployee_returnsSavedCsuEmployee_whenCallIsValid(){
        when(csuEmployeeService.saveCsuEmployee(any(CsuEmployee.class))).thenReturn(PAUL_WEST);
        CsuEmployee result = csuEmployeeController.saveCsuEmployee(new CsuEmployee());
        assertEquals("Paul",result.getFirstName());
    }

    @Test(expected = FieldMissingException.class)
    public void saveCsuEmployee_returnsFieldMissingException_whenFirstNameFieldIsMissing(){
        doThrow(FieldMissingException.class).when(csuEmployeeService).saveCsuEmployee(DAUGHTRY);
        CsuEmployee result = csuEmployeeController.saveCsuEmployee(DAUGHTRY);
    }

    @Test(expected = FieldMissingException.class)
    public void saveCsuEmployee_returnsFieldMissingException_whenLastNameFieldIsMissing(){
        doThrow(FieldMissingException.class).when(csuEmployeeService).saveCsuEmployee(CHER);
        CsuEmployee result = csuEmployeeController.saveCsuEmployee(CHER);
    }

    //PUT
    @Test
    public void updateCsuEmployee_returnsUpdatedCsuEmployee_whenCallIsValid(){
        when(csuEmployeeService.updateCsuEmployee(1L, SEAN_HAYES)).thenReturn(SEAN_HAYES);
        CsuEmployee result = csuEmployeeController.updateCsuEmployee(1L, SEAN_HAYES);
        assertEquals("Sean",result.getFirstName());
    }

    @Test(expected = FieldMissingException.class)
    public void updateCsuEmployee_returnsFieldMissingException_whenFirstNameFieldIsMissing(){
        doThrow(FieldMissingException.class).when(csuEmployeeService).updateCsuEmployee(2L, DAUGHTRY);
        CsuEmployee result = csuEmployeeController.updateCsuEmployee(2L, DAUGHTRY);
    }

    @Test(expected = FieldMissingException.class)
    public void updateCsuEmployee_returnsFieldMissingException_whenLastNameFieldIsMissing(){
        doThrow(FieldMissingException.class).when(csuEmployeeService).updateCsuEmployee(3L, CHER);
        CsuEmployee result = csuEmployeeController.updateCsuEmployee(3L, CHER);
    }

    //DELETE
    @Test
    public void deleteCsuEmployee_doNothing_whenCallIsValid(){
        doNothing().when(csuEmployeeService).deleteCsuEmployee(1L);
        csuEmployeeController.deleteCsuEmployee(1L);
        verify(csuEmployeeService, times(1)).deleteCsuEmployee(1L);
    }

    @Test(expected = CsuEmployeeNotFoundException.class)
    public void deleteCsuEmployee_returnsCsuEmployeeNotFoundException_whenArgumentOutOfRange(){
        doThrow(CsuEmployeeNotFoundException.class).when(csuEmployeeService).deleteCsuEmployee(3L);
        csuEmployeeController.deleteCsuEmployee(3L);
    }

}


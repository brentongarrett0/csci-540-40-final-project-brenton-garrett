package edu.csu.csuhumanresourcesapp;

import edu.csu.csuhumanresourcesapp.entities.CsuEmployee;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApplicationTests {

	public static final CsuEmployee SEAN_HAYES = CsuEmployee.builder()
			.employeeId(1L)
			.firstName("Sean")
			.lastName("Hayes")
			.jobTitle("Professor of Computer Science")
			.department("CS")
			.build();

	public static final CsuEmployee PAUL_WEST = CsuEmployee.builder()
			.employeeId(2L)
			.firstName("Paul")
			.lastName("West")
			.jobTitle("Professor of Humanities")
			.department("HIST")
			.build();

	public static final CsuEmployee CHER = CsuEmployee.builder()
			.employeeId(1L)
			.firstName("Cher")
			.lastName(null)
			.jobTitle("Professor of Computer Science")
			.department("CS")
			.build();

	public static final CsuEmployee DAUGHTRY = CsuEmployee.builder()
			.employeeId(1L)
			.firstName(null)
			.lastName("Daughtry")
			.jobTitle("Professor of Computer Science")
			.department("CS")
			.build();

	@Test
	public void contextLoads() {
	}

}

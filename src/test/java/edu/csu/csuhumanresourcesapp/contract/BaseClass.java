package edu.csu.csuhumanresourcesapp.contract;
import edu.csu.csuhumanresourcesapp.Application;
import edu.csu.csuhumanresourcesapp.controllers.CsuEmployeeController;
import edu.csu.csuhumanresourcesapp.entities.CsuEmployee;
import edu.csu.csuhumanresourcesapp.exceptions.CsuEmployeeNotFoundException;
import edu.csu.csuhumanresourcesapp.exceptions.FieldMissingException;
import edu.csu.csuhumanresourcesapp.services.CsuEmployeeService;
import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.Arrays;
import java.util.List;
import static edu.csu.csuhumanresourcesapp.ApplicationTests.*;
import static org.mockito.Mockito.*;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public abstract class BaseClass {

    @Mock
    CsuEmployeeService csuEmployeeService;

    @InjectMocks
    CsuEmployeeController csuEmployeeController;

    @Before
    public void setup()  {
        RestAssuredMockMvc.standaloneSetup(csuEmployeeController);

        //GET (all) - stub for getting multiple csu employees
        List<CsuEmployee> mockAllCsuEmployees =  Arrays.asList(SEAN_HAYES, PAUL_WEST);
        when(csuEmployeeService.getAllCsuEmployees()).thenReturn(mockAllCsuEmployees);

        //GET (1)
        when(csuEmployeeService.getCsuEmployee(1L)).thenReturn(SEAN_HAYES);
        when(csuEmployeeService.getCsuEmployee(2L)).thenThrow(CsuEmployeeNotFoundException.class);

        //POST
        when(csuEmployeeService.saveCsuEmployee(PAUL_WEST)).thenReturn(PAUL_WEST);
        doThrow(new FieldMissingException("Your request is missing required field 'firstName'."){}).when(csuEmployeeService).saveCsuEmployee(DAUGHTRY);
        doThrow(new FieldMissingException("Your request is missing required field 'lastName'."){}).when(csuEmployeeService).saveCsuEmployee(CHER);

        //PUT
        when(csuEmployeeService.updateCsuEmployee(1L, SEAN_HAYES)).thenReturn(SEAN_HAYES);
        doThrow(new FieldMissingException("Your request is missing required field 'firstName'."){}).when(csuEmployeeService).updateCsuEmployee(2L, DAUGHTRY);
        doThrow(new FieldMissingException("Your request is missing required field 'lastName'."){}).when(csuEmployeeService).updateCsuEmployee(3L, CHER);

        //DELETE
        doNothing().when(csuEmployeeService).deleteCsuEmployee(1L);
        doThrow(CsuEmployeeNotFoundException.class).when(csuEmployeeService).deleteCsuEmployee(2L);

    }

    @Test
    public void contextLoads() { }

}
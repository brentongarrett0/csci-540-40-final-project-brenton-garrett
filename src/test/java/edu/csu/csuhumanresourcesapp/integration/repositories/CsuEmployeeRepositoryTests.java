package edu.csu.csuhumanresourcesapp.integration.repositories;
import edu.csu.csuhumanresourcesapp.entities.CsuEmployee;
import edu.csu.csuhumanresourcesapp.exceptions.CsuEmployeeNotFoundException;
import edu.csu.csuhumanresourcesapp.repositories.CsuEmployeeRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import java.util.List;
import java.util.NoSuchElementException;
import static org.junit.Assert.*;
import static edu.csu.csuhumanresourcesapp.ApplicationTests.*;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest
public class CsuEmployeeRepositoryTests {

    @Autowired
    CsuEmployeeRepository csuEmployeeRepository;

    //GET (1)
    @Test
    @Sql("/save_csu_employee.sql")
    public void findById_returnsCsuEmployee_whenCallIsValid(){
        CsuEmployee result = csuEmployeeRepository.findByLastName("West");
        assertEquals("Paul", result.getFirstName());
    }

    @Test(expected = NoSuchElementException.class)
    public void findById_returnsNoSuchElementException_whenArgumentOutOfRange(){
        CsuEmployee result = csuEmployeeRepository.findById(-1L).get();
    }

    //GET (all)
    @Test
    @Sql("/save_csu_employees.sql")
    public void findAll_returnsCsuEmployee_whenCallIsValid(){
        List<CsuEmployee> result = (List<CsuEmployee>)csuEmployeeRepository.findAll();
        assertEquals(2, result.size());
    }

    //POST
    @Test
    @Sql("/clear_csu_employees.sql")
    public void save_returnsSavedCsuEmployee_whenCallIsValid(){
        CsuEmployee result = csuEmployeeRepository.save(SEAN_HAYES);
        assertEquals("Sean", result.getFirstName());
    }

    @Test (expected = DataIntegrityViolationException.class)
    @Sql("/clear_csu_employees.sql")
    public void save_returnsDataIntegrityViolationException_whenFirstNameFieldIsMissing(){
        CsuEmployee result = csuEmployeeRepository.save(DAUGHTRY);
    }

    @Test (expected = DataIntegrityViolationException.class)
    @Sql("/clear_csu_employees.sql")
    public void save_returnsDataIntegrityViolationException_whenLastNameFieldIsMissing(){
        CsuEmployee result = csuEmployeeRepository.save(CHER);
    }

    //PUT
    @Test
    @Sql("/save_csu_employee_with_typos.sql")
    public void update_returnsCsuEmployee_whenCallIsValid(){
            CsuEmployee updatedCsuEmployee = PAUL_WEST;
            updatedCsuEmployee.setEmployeeId(csuEmployeeRepository.findByLastName("Westt").getEmployeeId());
            CsuEmployee result = csuEmployeeRepository.save(updatedCsuEmployee);
            assertEquals("Paul", result.getFirstName());
    }

    @Test (expected = DataIntegrityViolationException.class)
    @Sql("/save_csu_employee.sql")
    public void update_returnsDataIntegrityViolationException_whenFirstNameFieldIsMissing(){
        CsuEmployee updatedCsuEmployee = DAUGHTRY;
        updatedCsuEmployee.setEmployeeId(csuEmployeeRepository.findByLastName("West").getEmployeeId());
        CsuEmployee result = csuEmployeeRepository.save(updatedCsuEmployee);
    }

    @Test (expected = DataIntegrityViolationException.class)
    @Sql("/save_csu_employee.sql")
    public void update_returnsDataIntegrityViolationException_whenLastNameFieldIsMissing(){
        CsuEmployee updatedCsuEmployee = CHER;
        updatedCsuEmployee.setEmployeeId(csuEmployeeRepository.findByLastName("West").getEmployeeId());
        CsuEmployee result = csuEmployeeRepository.save(updatedCsuEmployee);
    }

    //DELETE
    @Test
    @Sql("/save_csu_employees.sql")
    public void deleteById_returnsCsuEmployee_whenCallIsValid(){
        Long csuEmployeesId = csuEmployeeRepository.findByLastName("West").getEmployeeId();
        csuEmployeeRepository.deleteById(csuEmployeesId);
        List<CsuEmployee> allCsuEmployees = (List<CsuEmployee>)csuEmployeeRepository.findAll();
        assertEquals(1, allCsuEmployees.size());
    }

    @Test(expected = EmptyResultDataAccessException.class)
    public void deleteById_returnsEmptyResultDataAccessException_whenArgumentOutOfRange(){
        csuEmployeeRepository.deleteById(-1L);
    }

}

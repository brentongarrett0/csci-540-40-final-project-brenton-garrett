package edu.csu.csuhumanresourcesapp.e2e;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.csu.csuhumanresourcesapp.Application;
import edu.csu.csuhumanresourcesapp.entities.CsuEmployee;
import edu.csu.csuhumanresourcesapp.repositories.CsuEmployeeRepository;
import org.aspectj.internal.lang.reflect.DeclareAnnotationImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.support.DataAccessUtils;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.hamcrest.Matchers.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static edu.csu.csuhumanresourcesapp.ApplicationTests.*;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@SpringBootTest (webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT, classes = Application.class)
@AutoConfigureMockMvc
public class CsuHumanResourcesAppE2ETests {

    @Autowired
    private MockMvc mvc;

    @Autowired
    private CsuEmployeeRepository csuEmployeeRepository;

    //POST
    @Test
    public void postCsuEmployee_returnsSavedCsuEmployee_whenRequestIsValid() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.post("/csu-employees")
                .content(asJsonString(SEAN_HAYES))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.*" , hasSize(5)))
                .andExpect(jsonPath("$.firstName", is("Sean")))
                .andExpect(jsonPath("$.lastName", is("Hayes")))
                .andExpect(jsonPath("$.jobTitle", is("Professor of Computer Science")))
                .andExpect(jsonPath("$.department", is("CS")));
    }

    @Test
    public void postCsuEmployee_returns400StatusCode_whenfirstNameFieldIsMissing() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.post("/csu-employees")
                .content(asJsonString(DAUGHTRY))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    public void postCsuEmployee_returns400StatusCode_whenLastNameFieldIsMissing() throws Exception
    {
        mvc.perform(MockMvcRequestBuilders.post("/csu-employees")
                .content(asJsonString(CHER))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    //GET (all)
    @Test
    @Sql("/save_csu_employees.sql")
    public void getAllCsuEmployees_returnsAllCsuEmployees_whenRequestIsValid() throws Exception {

        mvc.perform(MockMvcRequestBuilders.get("/csu-employees")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].*" , hasSize(5)))
                .andExpect(jsonPath("$[0].firstName", is("Paul")));

    }

    //GET (1)
    @Test
    @Sql("/save_csu_employee.sql")
    public void getSpecificCsuEmployee_returnsSpecificCsuEmployee_whenRequestIsValid() throws Exception {
        CsuEmployee csuEmployee = csuEmployeeRepository.findByLastName("West");
        mvc.perform(MockMvcRequestBuilders.get(String.format("/csu-employees/%s",csuEmployee.getEmployeeId())))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.*" , hasSize(5)))
                .andExpect(jsonPath("$.firstName", is("Paul")))
                .andExpect(jsonPath("$.lastName", is("West")))
                .andExpect(jsonPath("$.jobTitle", is("Professor of Humanities")))
                .andExpect(jsonPath("$.department", is("HIST")));
    }

    //GET (1) - with out of range id
    @Test
    @Sql("/save_csu_employee.sql")
    public void getSpecificCsuEmployee_returns404StatusCode_whenIdIsOutOfRange() throws Exception {
        mvc.perform(MockMvcRequestBuilders.get("/csu-employees/10"))
                .andExpect(status().isNotFound());
    }

    //PUT
    @Test
    @Sql("/save_csu_employee_with_typos.sql")
    public void putCsuEmployee_returnsUpdatedCsuEmployee_whenRequestIsValid() throws Exception {
        CsuEmployee csuEmployee = PAUL_WEST;
        csuEmployee.setEmployeeId(csuEmployeeRepository.findByLastName("Westt").getEmployeeId());
        mvc.perform(MockMvcRequestBuilders.put(String.format("/csu-employees/%s",csuEmployee.getEmployeeId()))
                .content(asJsonString(csuEmployee))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.*" , hasSize(5)))
                .andExpect(jsonPath("$.firstName", is("Paul")))
                .andExpect(jsonPath("$.lastName", is("West")))
                .andExpect(jsonPath("$.jobTitle", is("Professor of Humanities")))
                .andExpect(jsonPath("$.department", is("HIST")));
    }

    @Test
    @Sql("/save_csu_employee.sql")
    public void putCsuEmployee_returns400StatusCode_whenFirstNameFieldIsMissing() throws Exception
    {
        CsuEmployee csuEmployee = CHER;
        csuEmployee.setEmployeeId(csuEmployeeRepository.findByLastName("West").getEmployeeId());
        mvc.perform(MockMvcRequestBuilders.put(String.format("/csu-employees/%s",csuEmployee.getEmployeeId()))
                .content(asJsonString(csuEmployee))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    @Sql("/save_csu_employee.sql")
    public void putCsuEmployee_returns400StatusCode_whenLastNameFieldIsMissing() throws Exception
    {
        CsuEmployee csuEmployee = DAUGHTRY;
        csuEmployee.setEmployeeId(csuEmployeeRepository.findByLastName("West").getEmployeeId());
        mvc.perform(MockMvcRequestBuilders.put(String.format("/csu-employees/%s",csuEmployee.getEmployeeId()))
                .content(asJsonString(csuEmployee))
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }


    //DELETE
    @Test
    @Sql("/save_csu_employee.sql")
    public void deleteCsuEmployee_returnsStatusCode200_whenRequestIsValid() throws Exception {
        Long csuEmployeesId = csuEmployeeRepository.findByLastName("West").getEmployeeId();
        mvc.perform(MockMvcRequestBuilders.delete(String.format("/csu-employees/%s",csuEmployeesId)))
                .andExpect(status().isOk());
    }

    @Test
    @Sql("/save_csu_employee.sql")
    public void deleteSpecificCsuEmployee_returns404StatusCode_whenIdIsOutOfRange() throws Exception {
        mvc.perform(MockMvcRequestBuilders.delete("/csu-employees/10"))
                .andExpect(status().isNotFound());
    }

    public static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
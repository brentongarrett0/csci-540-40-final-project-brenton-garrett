TRUNCATE TABLE csu_employees;
INSERT INTO csu_employees (first_name, last_name, job_title, department)
VALUES ('Paul', 'West' , 'Professor of Humanities' ,'HIST'),
('Sean', 'Hayes' , 'Professor of Computer Science' ,'CS');
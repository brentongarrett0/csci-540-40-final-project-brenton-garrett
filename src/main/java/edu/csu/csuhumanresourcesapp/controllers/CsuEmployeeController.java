package edu.csu.csuhumanresourcesapp.controllers;

import edu.csu.csuhumanresourcesapp.entities.CsuEmployee;
import edu.csu.csuhumanresourcesapp.exceptions.CsuEmployeeNotFoundException;
import edu.csu.csuhumanresourcesapp.services.CsuEmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
public class CsuEmployeeController {

    @Autowired
    private CsuEmployeeService csuEmployeeService;


    //greeting
    @GetMapping(path = "/greeting")
    public String getGreeting(){

        return "Title: CSCI 540 40 Final Project \n Created By: Brenton Garrett";
    }

    //GET 1
    @GetMapping(path = "/csu-employees/{id}")
    public CsuEmployee getCsuEmployee(@PathVariable(name = "id") Long id){
        return csuEmployeeService.getCsuEmployee(id);
    }

    // GET (all)
    @GetMapping(path = "/csu-employees")
    public List<CsuEmployee> getAllCsuEmployees(){
        return csuEmployeeService.getAllCsuEmployees();
    }

    //POST
    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping(path = "/csu-employees", consumes = "application/json", produces = "application/json")
    public CsuEmployee saveCsuEmployee(@RequestBody CsuEmployee csuEmployee){
        return csuEmployeeService.saveCsuEmployee(csuEmployee);
    }

    //PUT
    @PutMapping(path = "/csu-employees/{id}")
    public CsuEmployee updateCsuEmployee(@PathVariable(name = "id") Long id, @RequestBody CsuEmployee csuEmployee){
        return csuEmployeeService.updateCsuEmployee(id, csuEmployee);
    }

    //DELETE
    @DeleteMapping(path="/csu-employees/{id}")
    public void deleteCsuEmployee(@PathVariable(name = "id") Long id){
        csuEmployeeService.deleteCsuEmployee(id);
    }
}

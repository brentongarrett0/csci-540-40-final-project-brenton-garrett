package edu.csu.csuhumanresourcesapp.repositories;

import edu.csu.csuhumanresourcesapp.entities.CsuEmployee;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface CsuEmployeeRepository extends CrudRepository<CsuEmployee, Long> {
    @Query(value = "SELECT * FROM csu_employees WHERE last_name = :lastName", nativeQuery = true)
    CsuEmployee findByLastName(@Param("lastName") String lastName);
}
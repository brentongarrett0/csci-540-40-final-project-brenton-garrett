package edu.csu.csuhumanresourcesapp.entities;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Table(name="csu_employees")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CsuEmployee{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="employee_id")
    private Long employeeId;

    @Column(name="first_name", nullable = false)
    private String firstName;

    @Column(name="last_name", nullable = false)
    private String lastName;

    @Column(name="job_title")
    private String jobTitle;

    @Column(name="department")
    private String department;

}
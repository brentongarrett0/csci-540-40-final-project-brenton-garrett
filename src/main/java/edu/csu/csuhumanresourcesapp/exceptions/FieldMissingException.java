package edu.csu.csuhumanresourcesapp.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class FieldMissingException extends RuntimeException{
    public FieldMissingException(String message){
        super(message);
    }
}
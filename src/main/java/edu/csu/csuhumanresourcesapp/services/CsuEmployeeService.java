package edu.csu.csuhumanresourcesapp.services;
import edu.csu.csuhumanresourcesapp.entities.CsuEmployee;
import edu.csu.csuhumanresourcesapp.exceptions.CsuEmployeeNotFoundException;
import edu.csu.csuhumanresourcesapp.exceptions.FieldMissingException;
import edu.csu.csuhumanresourcesapp.repositories.CsuEmployeeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CsuEmployeeService {

    @Autowired
    private CsuEmployeeRepository csuEmployeeRepository;

    //GET (1)
    public CsuEmployee getCsuEmployee(Long id){
        CsuEmployee csuEmployee;
        try{ csuEmployee = csuEmployeeRepository.findById(id).get(); }
        catch(Exception e){
            throw new CsuEmployeeNotFoundException(String.format("There is no id of %s in the 'csu-employees' collection.", id));
        }
        return csuEmployee;

    }

    //GET (all)
    public List<CsuEmployee> getAllCsuEmployees(){
        return (List<CsuEmployee>)csuEmployeeRepository.findAll();
    }

    //POST
    public CsuEmployee saveCsuEmployee(CsuEmployee csuEmployee){
        if(csuEmployee.getFirstName() == null){
            throw new FieldMissingException("Your request is missing required field 'firstName'.");
        }
        if(csuEmployee.getLastName() == null){
            throw new FieldMissingException("Your request is missing required field 'lastName'.");
        }
        return csuEmployeeRepository.save(csuEmployee);
    }

    //UPDATE
    public CsuEmployee updateCsuEmployee(Long id, CsuEmployee csuEmployee){
        //id is out of range
        boolean idExists = csuEmployeeRepository.existsById(id);
        if(!idExists){
            throw new CsuEmployeeNotFoundException(String.format("There is no id of %s in the 'csu-employees' collection.", id));
        }

        //fields missing
        if(csuEmployee.getFirstName() == null){
            throw new FieldMissingException("Your request is missing required field 'firstName'.");
        }
        if(csuEmployee.getLastName() == null){
            throw new FieldMissingException("Your request is missing required field 'lastName'.");
        }

        csuEmployee.setEmployeeId(id);
        return csuEmployeeRepository.save(csuEmployee);
    }

    //DELETE
    public void deleteCsuEmployee(Long id){
        //id is out of range
        boolean idExists = csuEmployeeRepository.existsById(id);

        if(!idExists){
            throw new CsuEmployeeNotFoundException(String.format("There is no id of %s in the 'csu-employees' collection.", id));
        }

        csuEmployeeRepository.deleteById(id);
    }

}